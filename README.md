# Encryption Server #

This server contains a map of Id-Payload, which stores an encrypted content of bytes for any client specific Id.

It has been built specifically to serve the library in go_encrypt_client.

### Download the most updated version ###
$ git clone https://angelomastro@bitbucket.org/angelomastro/go_encrypt_server.git bitbucket.org/server


### Install it in your go-workspace ###
$ go get bitbucket.org/server

### Build it ###
$ go build bitbucket.org/server

Note: a binary called "server" has already been built in and you have it in this repo



### Run it ###
$ ./server