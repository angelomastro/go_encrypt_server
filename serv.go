package main

import (
    "encoding/json"
    "errors"
    "log"
    "net/http"
    "io/ioutil"
    "regexp"
    "strings"
)


// request, response schemas
type ReadRequest struct {
    Id     []byte 
}

type ReadResponse struct {
    Payload []byte
    Err     error
}

type WriteRequest struct {
    Id      []byte
    Payload []byte 
}

type WriteResponse struct {
    Err     error 
}

//in memory container of data: stringified []byte aesKey and payload
var aesKeyToPayload map[string]string 


//regex for URL
var strRead string = "/read" 
var rRead = regexp.MustCompile("/read/*") 

var strWrite string = "/write" 
var rWrite = regexp.MustCompile("/write/*") 


//response handler and writer functions 
func writeJsResponse(w http.ResponseWriter, content interface{}) {
//sends a Json response out of any object called "content"
    js, err := json.Marshal(content)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        log.Printf("error in writeJsonResponse\n")
        return
    }
    w.Header().Set("Content-Type", "text/plain")
    w.Write(js)
}


func handlerRead(w http.ResponseWriter, r *http.Request) {
    var jsonUrl = r.URL.Path[len(strRead)+1:]
    dec := json.NewDecoder(strings.NewReader(jsonUrl))
    
    var req ReadRequest
    var p []byte
    var e error
	if err := dec.Decode(&req); err != nil {
        log.Printf("Error in Decoding read request\n")
        e = err
	} else {
        if payload, found := aesKeyToPayload[string(req.Id)]; !found {
            e = errors.New("payload not found for this key\n")
        } else {
            e = nil
            p = []byte(payload)
        }
	}
    writeJsResponse(w, ReadResponse{p, e})
}

func handlerWrite(w http.ResponseWriter, r *http.Request) {
    body, _ := ioutil.ReadAll(r.Body)
    dec := json.NewDecoder(strings.NewReader(string(body)))
    
    var req WriteRequest
    var err error
	if err = dec.Decode(&req); err != nil {
        log.Printf("Error in Decoding write request\n")
        err = errors.New("Error in decoding write request\n")
	} else {
        aesKeyToPayload[string(req.Id)] = string(req.Payload)
        if _, found := aesKeyToPayload[string(req.Id)]; !found {
            log.Printf("Error in writing paylod in map\n")
            err = errors.New("failed to write payload in map\n")
        }

	}
    writeJsResponse(w, WriteResponse{err})
}


func route(w http.ResponseWriter, r *http.Request) {
    switch {
    case rRead.MatchString(r.URL.Path):
        handlerRead(w, r)
    case rWrite.MatchString(r.URL.Path):
        handlerWrite(w, r)
    default:
        w.Write([]byte("Unknown Pattern"))
    }
}



func main() {    
    aesKeyToPayload = make(map[string]string)
    
    http.HandleFunc("/", route) 
    http.ListenAndServe(":8080", nil)
}


